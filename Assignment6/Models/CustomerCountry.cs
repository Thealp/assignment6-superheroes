﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6.Models
{
    internal class CustomerCountry
    {
        public int Id { get; set; }
        public string Country { get; set; }
        public int Count { get; set; }
    }
}
