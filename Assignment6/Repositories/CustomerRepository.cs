﻿using Assignment6.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        public List<Customer> GetAllCustomers()
        {
            List<Customer> custList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            try
            {
                //Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            
                            while (reader.Read())
                            {
                                //Handle result
                                Customer temp = new Customer();

                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);

                                //Checking if the fields in database that is not set to "NOT NULL" have null values. 
                                if (!reader.IsDBNull(3))
                                {
                                    temp.Country = reader.GetString(3);
                                }
                               
                                if (!reader.IsDBNull(4))
                                {
                                    temp.PostalCode = reader.GetString(4);
                                }

                                if (!reader.IsDBNull(5))
                                {
                                     temp.Phone = reader.GetString(5);
                                }
                               
                                temp.Email = reader.GetString(6);
                                custList.Add(temp);
                            }
                        }
                    }
                    

                }

            }
            catch (SqlException ex)
            {
                //Log error (console.write elns)
                Console.WriteLine("feil: " + ex);
            }
            return custList;

        }

        public Customer GetCustomerById(int id)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer " +
                "WHERE CustomerId = @CustomerId";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);   
                                customer.FirstName = reader.GetString(1);   
                                customer.LastName = reader.GetString(2);

                                if (!reader.IsDBNull(3))
                                {
                                    customer.Country = reader.GetString(3);                           
                                }
                                if (!reader.IsDBNull(4))
                                {
                                    customer.PostalCode = reader.GetString(4);

                                }
                                if (!reader.IsDBNull(5))
                                {
                                    customer.Country = reader.GetString(5);
                                }
                                customer.Email = reader.GetString(6);

                            }
                        }
                    }
                }
            }
            catch(SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customer;
        }

        public Customer GetCustomerByName(string firstName, string lastName)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer " +
                "WHERE FirstName LIKE @FirstName AND LastName LIKE @LastName";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", firstName);
                        cmd.Parameters.AddWithValue("@LastName", lastName);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader.GetString(1);
                                customer.LastName = reader.GetString(2);

                                if (!reader.IsDBNull(3))
                                {
                                    customer.Country = reader.GetString(3);
                                }
                                if (!reader.IsDBNull(4))
                                {
                                    customer.PostalCode = reader.GetString(4);

                                }
                                if (!reader.IsDBNull(5))
                                {
                                    customer.Country = reader.GetString(5);
                                }
                                customer.Email = reader.GetString(6);

                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
            return customer;
        }

        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            string sql = "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES(@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false; 
                    }
                }

            }
            catch(Exception ex)
            {
                //Log
            }
            return success;

        }

        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;
            string sql = "UPDATE Customer " +
                "SET FirstName = @FirstName, LastName = @LastName, Country = @Country, " +
                "PostalCode = @PostalCode, Phone = @Phone, Email = @Email " +
                "WHERE CustomerId = @CustomerId";

            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }

            }
            catch (Exception ex)
            {
                //Log
            }
            return success;
        }

        //Legge i nytt repo
        public List<Customer> GetPageOfCustomers(int limit, int offset)
        {
            List<Customer> custList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer "+
                "ORDER BY CustomerId "+
                "OFFSET @Offset ROWS";
            try
            {
                //Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    //Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@Limit", limit);
                        cmd.Parameters.AddWithValue("@Offset", offset);
                        //Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {

                            while (reader.Read())
                            {
                                //Handle result
                                Customer temp = new Customer();

                                temp.CustomerId = reader.GetInt32(0);
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);

                                //Checking if the fields in database that is not set to "NOT NULL" have null values. 
                                if (!reader.IsDBNull(3))
                                {
                                    temp.Country = reader.GetString(3);
                                }

                                if (!reader.IsDBNull(4))
                                {
                                    temp.PostalCode = reader.GetString(4);
                                }

                                if (!reader.IsDBNull(5))
                                {
                                    temp.Phone = reader.GetString(5);
                                }

                                temp.Email = reader.GetString(6);
                                custList.Add(temp);
                            }
                        }
                    }


                }

            }
            catch (SqlException ex)
            {
                //Log error (console.write elns)
                Console.WriteLine("feil: " + ex);
            }
            return custList;
        }
            


     

       
    }
}
