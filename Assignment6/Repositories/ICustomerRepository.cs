﻿using Assignment6.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment6.Repositories
{
    public interface ICustomerRepository
    {
        //Read all customers
        public List<Customer> GetAllCustomers();

        //Read one customer with specified id
        public Customer GetCustomerById(int id);    

        //Read one customer with specified name
        public Customer GetCustomerByName(string firstName, string lastName);

        //return a page of customers
        public List<Customer> GetPageOfCustomers(int limit, int offset);

        //Add new customer
        public bool AddNewCustomer(Customer customer);

        //Update existing customer
        public bool UpdateCustomer(Customer customer);



        //Return number of customers in each country

        //
    }
}
