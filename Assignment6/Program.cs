﻿using Assignment6.Models;

using Assignment6.Repositories;

namespace Assignment6
{
    public class Program
    {
        public static void Main(string[] args)
        {

            ICustomerRepository repository = new CustomerRepository();
            //TestInsert(repository);
            TestSelectAPage(repository);
        }

        static void TestSelectAll(ICustomerRepository repository)
        {
            //will return a list of customers
            PrintCustomers(repository.GetAllCustomers());
        }

        static void TestSelect(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomerById(1));
           
        }
        static void TestSelectByName(ICustomerRepository repository)
        {
            PrintCustomer(repository.GetCustomerByName("Astrid", "Gruber"));
        }

        static void TestSelectAPage(ICustomerRepository repository) 
        {
            PrintCustomers(repository.GetPageOfCustomers(3, 6));
        }
        static void TestInsert(ICustomerRepository repository)
        {
            Customer test = new Customer()
            {
                FirstName = "Bob",
                LastName = "Hovelsen",
                Country = "Norway",
                PostalCode = "2050",
                Phone = "87659809",
                Email = "bhov@hotmail.com"
            };
            if (repository.AddNewCustomer(test))
            {
                Console.WriteLine("Yay, insert worked");
                
            }
            else
            {
                Console.WriteLine("Insert did not work");
            }
        }

        static void TestUpdate(ICustomerRepository repository)
        {
            Customer test = new Customer()
            {
                CustomerId = 4,
                FirstName = "Bjørn",
                LastName = "Hansen Olsen",
                Country = "Norway",
                PostalCode = "0171",
                Phone = "+47 22 44 22 22",
                Email = "bjorn.hansen@yahoo.no"
            };
            if (repository.UpdateCustomer(test))
            {
                Console.WriteLine("Yay, update worked");

            }
            else
            {
                Console.WriteLine("Update did not work");
            }

        }

        static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach(Customer customer in customers)
            {
                PrintCustomer(customer);
            }
        }

        static void PrintCustomer(Customer customer)
        {
            Console.WriteLine($"--- {customer.CustomerId} {customer.FirstName} {customer.LastName} {customer.Country} {customer.PostalCode} {customer.Phone} {customer.Email}---");
        }

        

        
    }   
}