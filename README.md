# Assignment6

## Table of contents
- Background
- Install
- Author

## Background
This project includes a directory of sql scripts called SuperheroesDb and a C# console application to access data from a Chinook databse. 
SuperheroesDb contains scripts to create database, create tables and linking tables (superhero, assistant, power, superheropower), inserting data, update data and delete data.
The console application contains a model for the customer table in the Chinook database, and repositories for interacting with the database. It contains functionality like:
- Reading all customers in database
- Read a customer based on ID 
- Read a customer based on Name
- Return a page of customers, using offset and limit as parameters
- Add customer
- Update customer

## Install
Clone repository in Visual Studio: git clone https://gitlab.com/Thealp/assignment6-superheroes.git 

## Author
Gitlab username: Thealp
[Gitlab-Link](https://gitlab.com/Thealp)

