Use SuperheroesDb

CREATE TABLE SuperheroPower(
	HeroId int,
	PowerId int,
	CONSTRAINT PK_SuperheroPower PRIMARY KEY (HeroId, PowerId)
)
ALTER TABLE SuperheroPower ADD CONSTRAINT [FK_SuperheroPowerHeroId]
    FOREIGN KEY ([HeroId]) REFERENCES [Superhero] ([ID]) 

ALTER TABLE SuperheroPower ADD CONSTRAINT [FK_SuperheroPowerPowerId]
    FOREIGN KEY ([PowerId]) REFERENCES [Power] ([ID]) 