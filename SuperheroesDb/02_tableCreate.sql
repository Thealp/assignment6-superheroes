Use SuperHeroesDb

CREATE TABLE Superhero (
	ID int PRIMARY KEY IDENTITY(1,1),
	HName nvarchar (50),
	Alias nvarchar (50),
	Origin nvarchar (255)
)

CREATE TABLE Assistant (
	ID int PRIMARY KEY IDENTITY(1,1),
	AName nvarchar (50)
)

CREATE TABLE Power (
	ID int PRIMARY KEY IDENTITY(1,1),
	PName nvarchar(50),
	PDescription nvarchar(255)
)