Use SuperheroesDb

INSERT INTO Power (PName, PDescription)
VALUES('Spiderweb', 'Can shoot spiderweb out of wrist')

INSERT INTO Power(PName, PDescription)
VALUES('Strength','Incredible strength')

INSERT INTO Power(PName, PDescription)
VALUES('Intelligence', 'Very smart')

INSERT INTO Power(PName, PDescription)
VALUES('Powered armor', 'Gived superhuman strength and durability, flight and weapons')

INSERT INTO SuperheroPower(HeroId, PowerId)
VALUES(1, 1)
INSERT INTO SuperheroPower(HeroId, PowerId)
VALUES(2, 1)
INSERT INTO SuperheroPower(HeroId, PowerId)
VALUES(3, 4)
INSERT INTO SuperheroPower(HeroId, PowerId)
VALUES(1, 3)
INSERT INTO SuperheroPower(HeroId, PowerId)
VALUES(3, 3)